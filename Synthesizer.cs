﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Audio;

namespace OtomataLib
{
    class Synthesizer
    {
        public Synthesizer()
        {
            // Create DynamicSoundEffectInstance object and start it
            _instance = new DynamicSoundEffectInstance(SampleRate, Channels == 2 ? AudioChannels.Stereo : AudioChannels.Mono);
            _instance.Play();

            // Create buffers
            const int bytesPerSample = 2;
            _xnaBuffer = new byte[Channels * SamplesPerBuffer * bytesPerSample];
            _workingBuffer = new float[Channels, SamplesPerBuffer];

            // Create voice structures
            _voicePool = new Voice[Polyphony];
            for (int i = 0; i < Polyphony; ++i)
            {
                _voicePool[i] = new Voice(this);
            }
            _freeVoices = new Stack<Voice>(_voicePool);
            _activeVoices = new List<Voice>();
        }

        // Synth Parameters

        public const int Channels = 1;
        public const int SampleRate = 44100;
        public const int SamplesPerBuffer = 2000;
        public const int Polyphony = 32;
        public int FadeInDuration = 300;
        public int FadeOutDuration = 300;
        public int NoteDuration = 0;
        public OscillatorDelegate Oscillator = OtomataLib.Oscillator.Square;

        // Public Methods

        public void Update()
        {
            foreach (Voice voice in _activeVoices)
            {
                if ((DateTime.Now - voice.StartTime).TotalMilliseconds >= NoteDuration + FadeInDuration)
                {
                    voice.Stop();
                }
            }

            while (_instance.PendingBufferCount < 2)
            {
                SubmitBuffer();
            }            
        }

        public void AddSound(float frequency)
        {
            Voice freeVoice = GetFreeVoice();

            if (freeVoice == null)
            {
                return;
            }
            
            freeVoice.Start(frequency);
            
            _activeVoices.Add(freeVoice);
        }

        public float Volume
        {
            get { return _instance.Volume; }
            set { _instance.Volume = value; }
        }

        // Private Methods

        private void SubmitBuffer()
        {
            ClearWorkingBuffer();
            FillWorkingBuffer();
            SoundHelper.ConvertBuffer(_workingBuffer, _xnaBuffer);
            _instance.SubmitBuffer(_xnaBuffer);
        }

        private void ClearWorkingBuffer()
        {
            Array.Clear(_workingBuffer, 0, Channels * SamplesPerBuffer);
        }

        private void FillWorkingBuffer()
        {
            // Call Process on all active voices
            for (int i = _activeVoices.Count - 1; i >= 0; --i)
            {
                Voice voice = _activeVoices[i];
                voice.Process(_workingBuffer);

                // Remove any voices that stopped being active
                if (!voice.IsAlive)
                {
                    _activeVoices.RemoveAt(i);
                    _freeVoices.Push(voice);
                }
            }
        }

        private Voice GetFreeVoice()
        {
            if (_freeVoices.Count == 0)
            {
                return null;
            }

            return _freeVoices.Pop();
        }

        private readonly DynamicSoundEffectInstance _instance;
        private readonly byte[] _xnaBuffer;
        private readonly float[,] _workingBuffer;

        private readonly Voice[] _voicePool;
        private readonly List<Voice> _activeVoices;
        private readonly Stack<Voice> _freeVoices;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OtomataLib
{
    public class Otomata
    {
        public enum Direction { Left, Right, Up, Down }

        public readonly int Size;

        private float[] Frequencies;
        private List<Cell> ActiveCells;

        internal int[,] CellCount;

        private Synthesizer Synth;

        public Otomata(int size)
        {
            Size = size;

            ActiveCells = new List<Cell>();
            CellCount = new int[Size, Size];

            Frequencies = new float[Size * 2];

            Synth = new Synthesizer();

            SetFrequences(new float[] {
                164.81377845643f, 
                246.94165062806f, 
                261.6255653006f, 
                293.66476791741f, 
                329.62755691287f, 
                369.99442271163f, 
                391.99543598175f, 
                493.88330125612f,
                587.32953583482f
            });
        }

        public void InitRandom(int seed, int cellCount)
        {
            ActiveCells.Clear();
            Random rnd = new Random(seed);

            for (int i = 0; i < cellCount; i++)
            {
                int x = rnd.Next(Size);
                int y = rnd.Next(Size);
                Direction dir = Direction.Up;
                switch (rnd.Next(4))
                {
                    case 0: dir = Direction.Down; break;
                    case 1: dir = Direction.Left; break;
                    case 2: dir = Direction.Right; break;
                    case 3: dir = Direction.Up; break;
                }
                AddCell(x, y, dir);
            }
        }

        public void AddCell(int x, int y, Otomata.Direction dir)
        {
            if (x < 0 || y < 0 || x >= Size || y >= Size)
                throw new ArgumentOutOfRangeException("Coordinates are not in the board");

            ActiveCells.Add(new Cell(this, x, y, dir));
            CellCount[x, y]++;
        }

        public void Reset()
        {
            ActiveCells.Clear();
        }

        public void SetOscillator(OscillatorDelegate oscillator)
        {
            Synth.Oscillator = oscillator;
        }

        public void SetFrequences(float[] frequences)
        {
            SetFrequences(frequences, frequences);
        }

        public void SetFrequences(float[] horizontal, float[] vertical)
        {
            if (horizontal.Length != Size || vertical.Length != Size)
                throw new ArgumentException("Length of frequency array does not match board size");

            for (int i = 0; i < Size; i++)
            {
                Frequencies[i] = horizontal[i];
                Frequencies[Size + i] = vertical[i];
            }
        }

        internal void Trigger(int x, int y, bool vertical)
        {
            if (vertical)
                Synth.AddSound(Frequencies[Size + y]);
            else
                Synth.AddSound(Frequencies[x]);
        }

        public void Update()
        {
            Synth.Update();
        }

        public void Step()
        {
            foreach (Cell cell in ActiveCells)
                cell.Move();
            foreach (Cell cell in ActiveCells)
                cell.PostMove();
        }

        public float Volume
        {
            get { return Synth.Volume; }
            set { Synth.Volume = value; }
        }

    }
}

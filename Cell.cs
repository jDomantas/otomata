﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OtomataLib
{
    internal class Cell
    {
        private int X, Y;
        private Otomata.Direction Direction;
        private Otomata Parent;

        internal Cell(Otomata parent, int x, int y, Otomata.Direction dir)
        {
            Parent = parent;
            X = x;
            Y = y;
            Direction = dir;
        }

        private void Flip()
        {
            if (Direction == Otomata.Direction.Down) Direction = Otomata.Direction.Up;
            else if (Direction == Otomata.Direction.Up) Direction = Otomata.Direction.Down;
            else if (Direction == Otomata.Direction.Left) Direction = Otomata.Direction.Right;
            else if (Direction == Otomata.Direction.Right) Direction = Otomata.Direction.Left;
        }

        internal void Move()
        {
            if ((X == 0 && Direction == Otomata.Direction.Left) ||
                (Y == 0 && Direction == Otomata.Direction.Up) ||
                (X == Parent.Size - 1 && Direction == Otomata.Direction.Right) ||
                (Y == Parent.Size - 1 && Direction == Otomata.Direction.Down))
            {
                Parent.Trigger(X, Y, Direction == Otomata.Direction.Left || Direction == Otomata.Direction.Right);
                Flip();
            }

            int dx = 0;
            int dy = 0;
            if (Direction == Otomata.Direction.Up) dy--;
            if (Direction == Otomata.Direction.Down) dy++;
            if (Direction == Otomata.Direction.Right) dx++;
            if (Direction == Otomata.Direction.Left) dx--;

            Parent.CellCount[X, Y]--;

            X += dx;
            Y += dy;

            Parent.CellCount[X, Y]++;
        }

        internal void PostMove()
        {
            if (Parent.CellCount[X, Y] > 1)
            {
                if (Direction == Otomata.Direction.Down) Direction = Otomata.Direction.Left;
                else if (Direction == Otomata.Direction.Left) Direction = Otomata.Direction.Up;
                else if (Direction == Otomata.Direction.Up) Direction = Otomata.Direction.Right;
                else if (Direction == Otomata.Direction.Right) Direction = Otomata.Direction.Down;
            }
        }
    }
}
